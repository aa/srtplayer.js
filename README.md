Basic SRT parsing / display support for in-browser use.

Uses an interval tree to efficiently manage titles when seeking.

Use
==========

```html
<video src="29228.meltyslides.mp4" controls id="video"></video>
<a href="29228.meltyslides.srt" id="src" class="srt" data-titles-for="video">sources</a>

<script src="dist/srtplayer.js"></script>

<script>
var titles = document.createElement("div");
document.body.appendChild(titles);
srtplayer.srtplayer(document.getElementById("video"), titles, document.getElementById("src").href);
</script>

```

Build
==========

	npm install
	npm run build







