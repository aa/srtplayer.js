var srtsplitpat = /^(?:(\d+)\r?\n)?((?:(?:\d\d):)?(?:\d\d):(?:\d\d)(?:[,.]\d{1,3})?)\s*-->\s*((?:(?:\d\d):)?(?:\d\d):(?:\d\d)(?:[,.]\d{1,3})?)?\s*$/m;


function parseTimeCode (tc) {
	var m = tc.match(/(\d\d):(\d\d):(\d\d)[.,](\d\d\d)/);
	if (m) {
		return (parseInt(m[1])*3600 + parseInt(m[2])*60 + parseInt(m[3]) + parseFloat("0."+m[4]));
	}
}

function parseSRT (text) {
	var parts = text.split(srtsplitpat),
		titles = [];
	for (var i=1, len =parts.length; i<len; i+=4) {
		titles.push({
			seq: (parts[i] != undefined) ? parseInt(parts[i]) : undefined,
			start: (parts[i+1] != undefined) ? parseTimeCode(parts[i+1]) : undefined,
			end: (parts[i+2] != undefined) ? parseTimeCode(parts[i+2]) : undefined,
			body: parts[i+3]
		});
	}
	return titles;
}

function normalizeSRT (titles) {
	for (var i=0, len=titles.length; i<len; i++) {
		var t = titles[i];
		if ((t.end == undefined) && (i+1 < len)) {
			t.end = titles[i+1].start;
		}
	}
	return titles;
}

module.exports.parseSRT = parseSRT;
module.exports.normalizeSRT = normalizeSRT;
